package edu.uni.fiis.poo.democontroller.Controller;

import edu.uni.fiis.poo.democontroller.Clases.Carrito;
import edu.uni.fiis.poo.democontroller.Clases.CarritoPrecioTotal;
import edu.uni.fiis.poo.democontroller.Clases.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.ArrayList;

@RestController
public class carritoController {
    @Autowired
    JdbcTemplate template;
    int contador_usuarios=6;
    @PostMapping("/registro_producto")
    public ResponseEntity registrarproducto(@RequestParam String nom
            ,@RequestParam String id_producto,@RequestParam float precio
            ,@RequestParam float cantidad ) throws Exception {

        String id_usuario = Integer.toString(contador_usuarios);
        Connection conn = template.getDataSource().getConnection();
        String sql = "INSERT INTO CARRITO(id_carrito, nombre_producto, id_producto,precio, cantidad) VALUES (?,?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, id_usuario);
        pst.setString(2, nom);
        pst.setString(3, id_producto);
        pst.setFloat(4, precio);
        pst.setFloat(5, cantidad);
        pst.executeUpdate();
        contador_usuarios++;
        return new ResponseEntity("Registrado Correctamente", HttpStatus.OK);
    }

    @RequestMapping(value="/lista_carrito")
    public ArrayList<Carrito> listarCarrito() throws SQLException {

        ArrayList<Carrito> carritos = new ArrayList<>();

        Connection conn = template.getDataSource().getConnection();

        String sql = "SELECT * FROM CARRITO";

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

            Carrito carrito;

            while (rs.next()) {
                carrito = new Carrito(rs.getString(2),rs.getString(3),rs.getFloat(4),rs.getFloat(5));
                carritos.add(carrito);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return carritos;
    }

    @RequestMapping(value="/precios_carrito")
    public ArrayList<CarritoPrecioTotal> listarPreciosCarrito() throws SQLException {

        ArrayList<CarritoPrecioTotal> carritos = new ArrayList<>();

        Connection conn = template.getDataSource().getConnection();

        String sql = "select PRECIO*CANTIDAD as PRECIO_TOTAL FROM CARRITO;";

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

            CarritoPrecioTotal carrito;

            while (rs.next()) {
                carrito = new CarritoPrecioTotal(rs.getFloat(1));
                carritos.add(carrito);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return carritos;
    }
}