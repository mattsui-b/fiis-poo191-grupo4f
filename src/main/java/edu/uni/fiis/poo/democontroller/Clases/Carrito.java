package edu.uni.fiis.poo.democontroller.Clases;

public class Carrito {

    public String nombre_prod;
    public String id_prod;
    public float precio;
    public float cant;

    public Carrito(String nombre_prod, String id_prod, float precio, float cant) {
        this.nombre_prod = nombre_prod;
        this.id_prod = id_prod;
        this.precio = precio;
        this.cant = cant;
    }
}
