package edu.uni.fiis.poo.democontroller.Controller;

import edu.uni.fiis.poo.democontroller.Clases.Producto;
import edu.uni.fiis.poo.democontroller.Clases.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;

@RestController
public class loginController {
        @Autowired
        JdbcTemplate template;

        @PostMapping("/iniciar_sesion")
        public ResponseEntity registrarProyecto(@RequestParam String username, @RequestParam String contr) throws Exception {


                Connection conn = template.getDataSource().getConnection();
                ModelAndView mav = new ModelAndView("/index.html");


                try {
                    String sql = "SELECT * FROM USUARIO";

                    Statement st;
                    ResultSet rs;
                    st = conn.createStatement();
                    rs = st.executeQuery(sql);

                    while (rs.next()) {
                        Usuario usuario = new Usuario(rs.getString(4),rs.getString(5));

                        if(usuario.user_name.equals(username)&&usuario.user_contra.equals(contr)){
                            String sql_1 = "INSERT INTO USER_ACTUAL(USERNAME) VALUES (?)";
                            PreparedStatement pst = conn.prepareStatement(sql_1);
                            pst.setString(1,username);
                            pst.executeUpdate();
                            return new ResponseEntity("Datos validados",HttpStatus.OK);
                        }
                        else return new ResponseEntity("Datos incorrectos",HttpStatus.EXPECTATION_FAILED);

                    }
                } catch (Exception e){
                    e.printStackTrace();}
                return new ResponseEntity("Fallo en la validadcion de daots",HttpStatus.BAD_GATEWAY);
        }


        @RequestMapping(value = "/user_logeado")
    public ArrayList<Usuario> listarUsuario_logeado() throws SQLException {

        ArrayList<Usuario> usuarios = new ArrayList<>();

        Connection conn = template.getDataSource().getConnection();

        String sql = "SELECT * FROM user_actual";

        Statement st;
        ResultSet rs;

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

            Usuario usuario;

            while (rs.next()) {
                usuario = new Usuario(rs.getString(1),null);
                usuarios.add(usuario);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return usuarios;
    }

    @RequestMapping (value = "/cerrar_sesion")
    public ResponseEntity cerrarSesion () throws SQLException {
        Connection conn = template.getDataSource().getConnection();

        String sql = "DELETE FROM USER_ACTUAL WHERE USERNAME IS NOT NULL";

            Statement st;
            ResultSet rs;
            try {
                st = conn.createStatement();
                rs = st.executeQuery(sql);

                } catch (SQLException e) {
                e.printStackTrace();
            }
        return new ResponseEntity("Sesión cerrada correctamente",HttpStatus.OK);
    }

}
