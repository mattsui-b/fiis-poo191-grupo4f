package edu.uni.fiis.poo.democontroller.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;

@RestController
public class ingresoDatosUsuario {
    @Autowired
    JdbcTemplate template;
    int contador_usuarios=4;
    @PostMapping("/registro_usuario")
    public ResponseEntity registrarProyecto(@RequestParam String nom
            ,@RequestParam String ape,@RequestParam String correo
            ,@RequestParam String dni ,@RequestParam String user,@RequestParam String con) throws Exception {

        String id_usuario = Integer.toString(contador_usuarios);
        Connection conn = template.getDataSource().getConnection();
        String sql = "INSERT INTO USUARIO(ID, NOMBRE, APELLIDO,DNI, CORREO,USERNAME,CONTRA) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, id_usuario);
        pst.setString(2, nom);
        pst.setString(3, ape);
        pst.setString(4, dni);
        pst.setString(5, correo);
        pst.setString(6, user);
        pst.setString(7, con);
        pst.executeUpdate();
        contador_usuarios++;
        return new ResponseEntity("Registrado Correctamente", HttpStatus.OK);
    }
}