package edu.uni.fiis.poo.democontroller.Clases;

import org.springframework.stereotype.Repository;



public class Producto {
    public String  id;
    public String nombre;
    public String precio;
    public String descripcion;
    public String imagen_source;

    public Producto(String id, String nombre, String precio, String descripcion, String imagen_source) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.imagen_source = imagen_source;
    }
}
