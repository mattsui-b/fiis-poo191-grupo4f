package edu.uni.fiis.poo.democontroller.Controller;


import edu.uni.fiis.poo.democontroller.Clases.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {
    @Autowired
    JdbcTemplate template;
    @RequestMapping (value="/lista_productos")
    public ArrayList<Producto> listarProductos() throws SQLException {

        ArrayList<Producto> productos = new ArrayList<>();

        Connection conn = template.getDataSource().getConnection();

        String sql = "SELECT * FROM PRODUCTO";

        Statement st;
        ResultSet rs;

       try {
           st = conn.createStatement();
           rs = st.executeQuery(sql);

           Producto producto;

           while (rs.next()) {
               producto = new Producto(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
               productos.add(producto);
           }
       } catch (Exception e){
           e.printStackTrace();
       }

     return productos;
    }
}
